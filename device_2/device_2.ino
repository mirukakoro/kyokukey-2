#include <Adafruit_Sensor_Calibration.h>
#include <Adafruit_AHRS.h>
#include <Keyboard.h>

Adafruit_Sensor *accelerometer;

#include "NXP_FXOS_FXAS.h"

Adafruit_NXPSensorFusion filter;

#if defined(ADAFRUIT_SENSOR_CALIBRATION_USE_EEPROM)
  Adafruit_Sensor_Calibration_EEPROM cal;
#else
  Adafruit_Sensor_Calibration_SDFat cal;
#endif

#define FILTER_UPDATE_RATE_HZ 100
#define PRINT_EVERY_N_UPDATES 10
#define AHRS_DEBUG_OUTPUT
#define IGNORE_TIME 500000

uint32_t timestamp;

void setup() {
  Serial.begin(115200);
  
  while (!Serial) yield();

  Serial.print("Keyboard…");
  Keyboard.begin();
  Serial.println("OK");

  if (!cal.begin()) {
    Serial.println("data-error:Failed to initialize calibration helper");
    while (1) delay(100);
  } else if (! cal.loadCalibration()) {
    Serial.println("data-error:No calibration loaded/found");
    while (1) delay(100);
  }

  if (!init_sensors()) {
    Serial.println("data-error:Failed to find sensors");
    while (1) delay(100);
  }
  
  accelerometer->printSensorDetails();

  setup_sensors();
  filter.begin(FILTER_UPDATE_RATE_HZ);
  timestamp = millis();

  Wire.setClock(400000); // 400KHz
}

float ax, ay, az, pax, pay, paz, sx, sy, sz, psx, psy, psz;
uint32_t at, pat;
uint32_t ignoreUntil;
float qw, qx, qy, qz;
float lax, lay, laz;
float grx, gry, grz;

void loop() {
  if ((millis() - timestamp) < (1000 / FILTER_UPDATE_RATE_HZ)) {
    return;
  }
  timestamp = millis();
  sensors_event_t accel;
  accelerometer->getEvent(&accel);

  cal.calibrate(accel);

  at = micros();
  ax = accel.acceleration.x;
  ay = accel.acceleration.y;
  az = accel.acceleration.z;
  uint32_t diff;
  diff = at-pat;
  sx = (ax-pax)/(diff/100000);
  sy = (ay-pay)/(diff/100000);
  sz = (az-paz)/(diff/100000);
  if (micros() > ignoreUntil){
    if (sx+sy+sz > 30) {
      Keyboard.press(KEY_LEFT_ARROW);
      Keyboard.releaseAll();
    } else if (sx+sy+sz > 10) {
      Keyboard.press(KEY_RIGHT_ARROW);
      Keyboard.releaseAll();
    } else {
      goto afterIgnoreUntil;
    }
    ignoreUntil = micros() + IGNORE_TIME;
    afterIgnoreUntil:;
  }
  pat = at;
  pax = ax;
  pay = ay;
  paz = az;
  psx = sx;
  psy = sy;
  psz = sz;
}