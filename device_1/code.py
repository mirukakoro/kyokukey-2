# for NeoPixel
import time
import neopixel
import board
# for keyboard
import usb_hid
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
# for analog input
from analogio import AnalogIn

import keycodes # keycodes

INPUT_INTERVAL = 0.1

pixels = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=0.3, auto_write=True)

keyboard = Keyboard(usb_hid.devices)

sensor_pins = [
    AnalogIn(board.A1),
    AnalogIn(board.A2),
    AnalogIn(board.A3),
    AnalogIn(board.A4),
    #AnalogIn(board.A5),
]
sensor_opts = [(42000, 46000)]*5


def get_raw_inputs():
    return list(sensor_pin.value for i, sensor_pin in enumerate(sensor_pins))


def get_inputs():
    values = get_raw_inputs()
    for i, value in enumerate(values):
        opt = sensor_opts[i]
        a = False
        for j, threshold in enumerate(opt):
            if value <= threshold:
                values[i] = j
                a = True
                break
        if not a:
            values[i] = len(opt)
    return values


prev_time = time.monotonic_ns()
while 1:
    now_time = time.monotonic_ns()
    value = get_inputs()
    keycodes.do_action(value)
    prev_time = now_time
    time.sleep(INPUT_INTERVAL)
