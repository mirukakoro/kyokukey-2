def do_action(values):
    print(values)
    if sum(values) == 0:
        return
    codes = []
    # https://github.com/adafruit/Adafruit_CircuitPython_HID/blob/d5b6ea50f49ef4575a1fbe52259c2dff44bcab2b/adafruit_hid/keycode.py
    codes += {
        #L  n  M  P fingers
        (0, 0, 0, 0): [],
        (0, 0, 0, 1): [Keycode.A],
        (0, 0, 0, 2): [Keycode.B],
        (0, 0, 1, 0): [Keycode.C],
        (0, 0, 1, 1): [Keycode.D],
        (0, 0, 1, 2): [Keycode.E],
        (0, 0, 2, 0): [Keycode.F],
        (0, 0, 2, 1): [Keycode.G],
        (0, 0, 2, 2): [Keycode.H],
        (0, 1, 0, 0): [Keycode.I],
        (0, 1, 0, 1): [Keycode.J],
        (0, 1, 0, 2): [Keycode.K],
        (0, 1, 1, 0): [Keycode.L],
        (0, 1, 1, 1): [Keycode.M],
        (0, 1, 1, 2): [Keycode.N],
        (0, 1, 2, 0): [Keycode.O],
        (0, 1, 2, 1): [Keycode.P],
        (0, 1, 2, 2): [Keycode.Q],
        (0, 2, 0, 0): [Keycode.R],
        (0, 2, 0, 1): [Keycode.S],
        (0, 2, 0, 2): [Keycode.T],
        (0, 2, 1, 0): [Keycode.U],
        (0, 2, 1, 1): [Keycode.V],
        (0, 2, 1, 2): [Keycode.W],
        (0, 2, 2, 0): [Keycode.X],
        (0, 2, 2, 1): [Keycode.Y],
        (0, 2, 2, 2): [Keycode.Z],
        
        (1, 0, 0, 0): [Keycode.ONE],
        (1, 0, 0, 1): [Keycode.TWO],
        (1, 0, 0, 2): [Keycode.THREE],
        (1, 0, 1, 0): [Keycode.FOUR],
        (1, 0, 1, 1): [Keycode.FIVE],
        (1, 0, 1, 2): [Keycode.SIX],
        (1, 0, 2, 0): [Keycode.SEVEN],
        (1, 0, 2, 1): [Keycode.EIGHT],
        (1, 0, 2, 2): [Keycode.NINE],
        (1, 1, 0, 0): [Keycode.ZERO],

        (1, 1, 0, 1): [Keycode.ENTER],
        (1, 1, 0, 2): [Keycode.ESCAPE],
        (1, 1, 1, 0): [Keycode.BACKSPACE],
        (1, 1, 1, 1): [Keycode.TAB],
        (1, 1, 1, 2): [Keycode.SPACE],

        (1, 1, 2, 0): [Keycode.MINUS],
        (1, 1, 2, 1): [Keycode.EQUALS],
        (1, 1, 2, 2): [Keycode.LEFT_BRACKET],
        (1, 2, 0, 0): [Keycode.RIGHT_BRACKET],
        (1, 2, 0, 1): [Keycode.BACKSLASH],
        (1, 2, 0, 2): [Keycode.POUND],
        (1, 2, 1, 0): [Keycode.SEMICOLON],
        (1, 2, 1, 1): [Keycode.QUOTE],
        (1, 2, 1, 2): [Keycode.GRAVE_ACCENT],
        (1, 2, 2, 0): [Keycode.COMMA],
        (1, 2, 2, 1): [Keycode.PERIOD],
        (1, 2, 2, 2): [Keycode.FORWARD_SLASH],
    }.get(tuple(values[:4]), [])
    print(f'{values} → {codes}')
    keyboard.send(*codes)
